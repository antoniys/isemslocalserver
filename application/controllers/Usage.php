<?php

Class Usage extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->database();
		date_default_timezone_set('Asia/Jakarta');
	}
	
	function index() {
		$free = shell_exec('free');
		$free = (string)trim($free);
		$free_arr = explode("\n", $free);
		$mem = explode(" ", $free_arr[1]);
		$mem = array_filter($mem);
		$mem = array_merge($mem);
		$memory_usage = round($mem[2]/$mem[1]*100,0)."%";
		
		$temperature = shell_exec("/home/pi/temp");
		
		$minipc = $this->db->query("select * from mini_pc");
		foreach ($minipc->result() as $row) {
			$code = $row->code;
			$this->db->query("INSERT INTO pemakaian_daya(code,temperature,memory) VALUES ('$code','$temperature','$memory_usage') " );
		}
		$this->load->view("usage");
		
	}
}
