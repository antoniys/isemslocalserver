<?php
Class Getting_started extends CI_Controller{

    var $API ="";

    function __construct() {
        parent::__construct();
        // $this->API="https://sisca2019.000webhostapp.com/index.php/";
        $this->API="https://www.interlog.co.id/";
        // $this->API="http://localhost/isems/";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
    }

    function index()
    {
      $insert =  $this->curl->simple_get($this->API.'/kontak');
      $data['kontak'] = json_decode($insert);
      $this->load->view("getting_started",$data);
      // echo $insert;
    }

    function update()
    {
      $kode_mini_pc = $this->input->post('kode_mini_pc');


      // 1. Create Table Chiller
      $sql_chiller = "CREATE TABLE `chiller` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `teks` varchar(120) NOT NULL,
      `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
      )";

      if ($this->db->query($sql_chiller) === TRUE) 
      {
        echo "<p class='sukses'>Table Chiller Berhasil Di Create... </p>";
      }else{
        echo "<p class='gagal'>Table Chiller Gagal Di Create</p>";
      }

      // 2. Create Table Data Chiller
      $sql_data_chiller = "CREATE TABLE `data_chiller` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `device_code` varchar(6) DEFAULT NULL,
      `volt` char(1) DEFAULT NULL,
      `date_inserted` datetime DEFAULT CURRENT_TIMESTAMP,
      `merk_unit` int(3) DEFAULT NULL,
      `type_unit` varchar(100) DEFAULT NULL,
      `ampere` float DEFAULT NULL,
      `kode_unit` int(2) DEFAULT NULL,
      PRIMARY KEY (`id`)
      )";

      if ($this->db->query($sql_data_chiller) === TRUE) 
      {
        echo "<p class='sukses'> Table Data Chiller Berhasil Di Create... </p>";
      }else{
        echo "<p class='gagal'>Table Data Chiller Gagal Di Create</p>";
      }

      // 3. Create Table Fase
      $sql_fase = "CREATE TABLE `fase` (
      `value` int(11) NOT NULL DEFAULT '1',
      `date_updated` datetime DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`value`))";
      
      if ($this->db->query($sql_fase) === TRUE) 
      {
        echo "<p class='sukses'> Table Fase Berhasil Di Create Dengan Nilai Default... </p>";
        $this->db->query("insert into fase (value) values ('1')");
      }else{
        echo "<p class='gagal'>Table Fase Gagal Di Create</p>";
      } 

      // 4. Create Table Mini PC
      $sql_mini_pc = "CREATE TABLE `mini_pc` (
      `code` varchar(6) NOT NULL,
      PRIMARY KEY (`code`))";

      if ($this->db->query($sql_mini_pc) === TRUE) 
      {
        echo "<p class='sukses'> Table Mini PC Berhasil Di Create Dengan Kode Mini PC yg di input : $kode_mini_pc... </p>";
        $this->db->query("insert into mini_pc (code) values ('$kode_mini_pc')");
      }else{
        echo "<p class='gagal'>Table Mini PC Gagal Di Create</p>";
      }

      // 5. Create Table Penampung Produksi
      $sql_produksi = "CREATE TABLE `penampung_produksi` (
      `kode_vm` varchar(6) NOT NULL,
      `kode_mini_pc` varchar(6) DEFAULT NULL,
      `kode_toko` int(1) DEFAULT NULL,
      `kode_installer` int(3) DEFAULT NULL,
      `tanggal_instalasi` datetime DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`kode_vm`))";

      if ($this->db->query($sql_produksi) === TRUE) 
      {
        echo "<p class='sukses'> Table Penampung Produksi Berhasil Di Create... </p>";
      }else{
        echo "<p class='gagal'>Table Penampung Produksi Gagal Di Create</p>";
      }

      // 6. Create Table Recording
      $sql_recording = "CREATE TABLE `recording` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `message` varchar(300) DEFAULT NULL,
      `date_updated` datetime DEFAULT CURRENT_TIMESTAMP,
      `status` char(6) DEFAULT NULL,
      PRIMARY KEY (`id`))";

      if ($this->db->query($sql_recording) === TRUE) 
      {
        echo "<p class='sukses'> Table Recording Berhasil Di Create... </p>";
      }else{
        echo "<p class='gagal'>Table Recording Gagal Di Create</p>";
      } 

      // 7. Create Table Testing
      $sql_testing = "CREATE TABLE `testing` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `kode` varchar(45) DEFAULT NULL,
      `sinyal` varchar(5) DEFAULT NULL,
      PRIMARY KEY (`id`)
      )";

      if ($this->db->query($sql_testing) === TRUE) 
      {
        echo "<p class='sukses'> Table Testing Berhasil Di Create... </p>";
      }else{
        echo "<p class='gagal'>Table Testing Gagal Di Create</p>";
      } 

      // 8. Create Table Volt Meter
      $sql_volt_meter = "CREATE TABLE `volt_meter` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `teks` varchar(120) DEFAULT NULL,
      `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`))";

      if ($this->db->query($sql_volt_meter) === TRUE) 
      {
        echo "<p class='sukses'> Table Volt Meter Berhasil Di Create... </p>";
      }else{
        echo "<p class='gagal'>Table Volt Meter Gagal Di Create</p>";
      } 

      // 9. Create Table Installer
      $sql_installer = "CREATE TABLE `tb_installer` (
      `id` int(11) NOT NULL ,
      `nama` varchar(45) NOT NULL,
      `nik` varchar(45) NOT NULL,
      PRIMARY KEY (`id`))";

      if ($this->db->query($sql_installer) === TRUE) 
      {
        echo "<p class='sukses'> Table Installer Berhasil Di Create & Sync Data Ke Server... </p>";

        $installer =  $this->curl->simple_get($this->API.'/Mini_pc_installer');
        $installer = json_decode($installer);
        foreach($installer as $row)
        {
          $this->db->query("insert into tb_installer (id,nama,nik) values ('$row->id','$row->nama','$row->nik')");
        }
      }else{
        echo "<p class='gagal'>Table  Installer Gagal Di Create</p>";
      }

      // 10. Create Table Merek Chiller
      $sql_merek_chiller = "CREATE TABLE `tb_merek_chiller` (
      `id` int(11) NOT NULL,
      `merek_chiller` varchar(50) NOT NULL,
      `keterangan` varchar(300) NOT NULL,
      PRIMARY KEY (`id`))";

      if ($this->db->query($sql_merek_chiller) === TRUE) 
      {
        echo "<p class='sukses'> Table Merek Chiller Berhasil Di Create & Sync Data Ke Server... </p>";
        $merk =  $this->curl->simple_get($this->API.'/Mini_pc_merk');
        $merk = json_decode($merk);
        foreach($merk as $row)
        {
          $this->db->query("insert into tb_merek_chiller (id,merek_chiller,keterangan) values ('$row->id','$row->merek_chiller','$row->keterangan')");
        }
      }else{
        echo "<p class='gagal'>Table  Merek Chiller Gagal Di Create</p>";
      }

      // 11. Create Table Standar
      $standar = "CREATE TABLE `tb_standar2` (
      `id` int(11) NOT NULL,
      `code_id` varchar(10) NOT NULL,
      `remark` varchar(45) DEFAULT NULL,
      `jenis_pendingin` varchar(45) DEFAULT NULL,
      PRIMARY KEY (`id`))";

      if ($this->db->query($standar) === TRUE) 
      {
        echo "<p class='sukses'> Table Standar Berhasil Di Create & Sync Data Ke Server... </p>";
        $standar =  $this->curl->simple_get($this->API.'/Mini_pc_standar');
        $standar = json_decode($standar);
        foreach($standar as $row)
        {
          $this->db->query("insert into tb_standar2 (id,code_id,remark,jenis_pendingin) values ('$row->id','$row->code_id','$row->remark','$row->jenis_pendingin')");
        }
        }else{
          echo "<p class='gagal'>Table  Standar Gagal Di Create</p>";
        }

        // 12. Create Table Toko
        $sql_toko = "CREATE TABLE `tb_toko` (
        `id` int(11) NOT NULL ,
        `nama_toko` varchar(45) NOT NULL,
        `kode_toko` varchar(10) DEFAULT NULL,
        PRIMARY KEY (`id`)
        )";

        if ($this->db->query($sql_toko) === TRUE) 
        {
          echo "<p class='sukses'> Table Toko Berhasil Di Create & Sync Data Ke Server... </p>";
          $toko =  $this->curl->simple_get($this->API.'/Mini_pc_toko');
          $toko = json_decode($toko);
          foreach($toko as $row)
          {
            $this->db->query("insert into tb_toko (id,nama_toko,kode_toko) values ('$row->id','$row->nama_toko','$row->kode_toko')");
          }
        }else{
          echo "<p class='gagal'>Table  Toko Gagal Di Create</p>";
        } 

        // 13. Create Table Local Chiller
        $sql_volt_meter = "CREATE TABLE `local_chiller` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `device_code` varchar(10) NOT NULL,
        `suhu_in` float DEFAULT NULL,
        `suhu_out` float DEFAULT NULL,
        `humidity` float DEFAULT NULL,
        `ampere` float DEFAULT NULL,
        `sinyal` float DEFAULT NULL,
        `durasi` int(3) DEFAULT NULL,
        `counter` int(3) DEFAULT NULL,
        `nomor_ip` varchar(30) DEFAULT NULL,
        `date_insert_local` datetime DEFAULT NULL,
        `date_insert_center` datetime DEFAULT CURRENT_TIMESTAMP,
        `jumlah_data` int(11) DEFAULT NULL,
        `volt` char(1) DEFAULT 't',
        `doors` int(1) NOT NULL,
        `penampung` int(1) NOT NULL,
        `keterangan` varchar(100) NOT NULL,
        PRIMARY KEY (`id`)
        )";

        if ($this->db->query($sql_volt_meter) === TRUE) 
        {
          echo "<p class='sukses'> Table Local Chiller Berhasil Di Create... </p>";
        }else{
          echo "<p class='gagal'>Table Local Chiller Gagal Di Create</p>";
        } 

        // 14. Create Table Local Volt Meter
        $sql_volt_meter = "CREATE TABLE `local_volt_meter` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `device_code` varchar(10) NOT NULL,
        `suhu_in` float DEFAULT NULL,
        `volt_r` int(3) DEFAULT NULL,
        `volt_s` int(3) DEFAULT NULL,
        `volt_t` int(3) DEFAULT NULL,
        `sinyal` float DEFAULT NULL,
        `ip` float DEFAULT NULL,
        `date_insert_local` datetime DEFAULT NULL,
        `date_insert_center` datetime DEFAULT CURRENT_TIMESTAMP,
        `penampung` int(1) NOT NULL,
        PRIMARY KEY (`id`)
        )";

        if ($this->db->query($sql_volt_meter) === TRUE) 
        {
          echo "<p class='sukses'> Table Local Volt Meter Berhasil Di Create... </p>";
        }else{
          echo "<p class='gagal'>Table Local Volt Meter Gagal Di Create</p>";
        } 

        // 15. Create Table Browser Local
        $browser_local = "CREATE TABLE `browser_local` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `variabel` varchar(6) DEFAULT NULL,
        `nilai` int(1) DEFAULT NULL,
        PRIMARY KEY (`id`)
        )";

        if ($this->db->query($browser_local) === TRUE) 
        {
          echo "<p class='sukses'> Table Browser Local Berhasil Di Create... </p>";
        }else{
          echo "<p class='gagal'>Table Browser Local Gagal Di Create</p>";
        }

        // 16. Create Table Fitur
        $fitur = "CREATE TABLE `fitur` (
        `id` int(2) NOT NULL AUTO_INCREMENT,
        `variabel` varchar(6) DEFAULT NULL,
        `nilai` varchar(45) DEFAULT NULL,
        PRIMARY KEY (`id`)
        )";

        if ($this->db->query($fitur) === TRUE) 
        {
          echo "<p class='sukses'> Table Fitur Berhasil Di Create... </p>";
        }else{
          echo "<p class='gagal'>Table Fitur Gagal Di Create</p>";
        }

        // 17. Create Table Pemakaian Daya
        $pemakaian_daya = "CREATE TABLE `pemakaian_daya` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `code` varchar(8) DEFAULT NULL,
        `temperature` varchar(8) DEFAULT NULL,
        `memory` varchar(8) DEFAULT NULL,
        `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
        )";

        if ($this->db->query($pemakaian_daya) === TRUE) 
        {
          echo "<p class='sukses'> Table Pemakaian Daya Berhasil Di Create... </p>";
        }else{
          echo "<p class='gagal'>Table Pemakaian Daya Gagal Di Create</p>";
        }

    }
}