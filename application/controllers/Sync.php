<?php
Class Sync extends CI_Controller{

    var $API ="";

    function __construct() {
        parent::__construct();
        // $this->API="http://localhost/isems/index.php/";
        $this->API="https://interlog.co.id/";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
    }

    function index(){
      $jam = date("H");
        $query = $this->db->query("select id from chiller order by id desc limit 0,1");
        $jumlah = $query->num_rows();
        if($jumlah==0)
        {
          $id=0;
        }else{
          foreach($query->result() as $row)
          {
            $id = $row->id;
          }
        }
        

        $query = $this->db->query("select id from volt_meter order by id desc limit 0,1");
        $jumlah_volt_meter = $query->num_rows();
        if($jumlah_volt_meter==0)
        {
          $id2=0;
        }else{
          foreach($query->result() as $row)
          {
            $id2 = $row->id;
          }
        }
        
          $data['data_chiller'] = json_decode($this->curl->simple_get($this->API."/Sync_local_chiller?id=$id"));
          $data['data_volt_meter'] = json_decode($this->curl->simple_get($this->API."/Sync_local_volt_meter?id=$id2"));
           $this->load->view('sync',$data);

 
    }

}