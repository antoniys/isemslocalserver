<?php
Class Volt extends CI_Controller{

    var $API ="";

    function __construct() {
        parent::__construct();
        // $this->API="https://sisca2019.000webhostapp.com/index.php/";
        $this->API="http://localhost/interlog-api/index.php/";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
    }

    function index(){
      //Fungsi untuk sync data volt meter
      $suhu_in2 = 0;
      $volt_r = 0;
      $volt_s = 0;
      $volt_t = 0;
      $no2 = 0;
      $id2 = 0;
      $query3 = $this->db->query("select * from volt_meter");
      $jumlah_volt = $query3->num_rows();
      if($jumlah_volt>=1)
      {
        foreach($query3->result() as $row3)
        {
          $teks2 = explode("~", $row3->teks);
          $no2 = $no2 + 1;
          $id2 = "$id2".","."$row3->id";
          $device_id2 = $teks2[0];
          $suhu_in2 = $teks2[1] + $suhu_in2;
          $volt_r = $teks2[2] + $volt_r;
          $volt_s = $teks2[3] + $volt_s;
          $volt_t = $teks2[4] + $volt_t;
          $signal2 = $teks2[5];
          $ip2 = $teks2[6];
          $date_insert_local2 = $row3->created_date; 
        }  
          $suhu_in2 = $suhu_in2 / $no2;
          $volt_r = $volt_r / $no2;
          $volt_s = $volt_s / $no2;
          $volt_t = $volt_t / $no2;    

          $data3 = array(
            'device_code' => $device_id2,
            'suhu_in'     => $suhu_in2,
            'volt_r'    => $volt_r,
            'volt_s'    => $volt_s,
            'volt_t'      => $volt_t,
            'sinyal'      => $signal2,
            'ip'      => $ip2,
            'date_insert_local' => $date_insert_local2,
          );
          echo "<hr>";
            $insert_volt =  $this->curl->simple_post($this->API.'/Volt', $data3, array(CURLOPT_BUFFERSIZE => 10));
            if($insert_volt="")
            {
              $masuk_local_volt = $this->db->insert('local_volt_meter', $data3);
            }
            // $hapus_local = $this->db->query("delete from volt_meter where id in ($id2)");
            echo $insert_volt;
      }

      $this->load->view('sync');
    } 
}