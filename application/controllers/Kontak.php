<?php
Class Kontak extends CI_Controller{

    var $API ="";

    function __construct() {
        parent::__construct();
        // $this->API="https://sisca2019.000webhostapp.com/index.php/";
        $this->API="https://www.interlog.co.id/";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
    }

    function index(){
      $id = 0;
      $data = array();

      $index = 0;
      $query = $this->db->query("select * from data_chiller");
      $jumlah_data = $query->num_rows();
      
      if($jumlah_data >=1)
      {
        foreach($query->result() as $row)
        {
          
          $device_code = $row->device_code;
          $device_volt = $row->volt;
          $pesan = "";
          // echo "Kode Alat : $device_code <br>";
          $query2 = $this->db->query("select * from chiller where left(teks,6)='$device_code' order by id asc");
          $jumlah_data2 = $query2->num_rows();
          // echo "Jumlah Data kode $device_code : $jumlah_data2 <br>";
          if($jumlah_data2 >=1)
          {
            $counter = 0;
            $no=0;
            $no2=0;
            $suhu_in = 0;
            $suhu_out = 0;
            $humidity = 0;
            $ampere = 0;
            $signal = 0;
            $durasi = 0;
            $door = 0;
            foreach($query2->result() as $row2)
            {
              $teks = explode("~", $row2->teks);
              $no = $no+1;
              $id = "$id".","."$row2->id";
              $device_id = $device_code;
              $signal = $teks[5] + $signal;
              $durasi = $durasi + $teks[6];

              //mengambil nilai error dari sensor
              $ec = $teks[8];
              $error_code = str_split($ec);
              $error_suhu_in = $error_code[0];
              $error_suhu_out = $error_code[1];
              $error_humidity = $error_code[2];
              $error_ampere = $error_code[3];
              
              //mengambil keputusan error pada sensor suhu in
              if($error_suhu_in=='0')
              {
                 $pesan = "$pesan"." Suhu In ERROR";
                // Mengambil data temporary
                $query_temp = $this->db->query("select suhu_in from local_chiller where device_code='$device_id' and penampung='1'");
                foreach($query_temp->result() as $row_temp)
                {
                  $suhu_in = $row_temp->suhu_in + $suhu_in;
                  //echo "SUhu IN : $suhu_in <br>";
                }
              }else
              {
                $suhu_in = $teks[1] + $suhu_in;
              }

              //mengambil keputusan error pada sensor suhu out
              if($error_suhu_out=='0')
              {
                $pesan = "$pesan"." Suhu Out ERROR";
                // Mengambil data temporary
                $query_temp = $this->db->query("select suhu_out from local_chiller where device_code='$device_id' and penampung='1'");
                foreach($query_temp->result() as $row_temp)
                {
                  $suhu_out = $row_temp->suhu_out + $suhu_out;
                  
                }
              }else
              {
                $suhu_out = $teks[2] + $suhu_out;
              }   

              //mengambil keputusan error pada sensor humidity
              if($error_humidity=='0')
              {
                $pesan = "$pesan"." Humidity ERROR";
                // Mengambil data temporary
                $query_temp = $this->db->query("select humidity from local_chiller where device_code='$device_id' and penampung='1'");
                foreach($query_temp->result() as $row_temp)
                {
                  $humidity = $row_temp->humidity + $humidity;
                  
                }
              }else
              {
                if($teks[3] >=99 and $teks[7]== 0) 
                {
                  $pesan = "$pesan"." Humidity ERROR";
                  $query_temp = $this->db->query("select humidity from local_chiller where device_code='$device_id' and penampung='1'");
                  foreach($query_temp->result() as $row_temp)
                  {
                    $humidity = $row_temp->humidity + $humidity;
                    
                  }
                }else{
                  $humidity = $teks[3] + $humidity;
                }
                
              }

              //mengambil keputusan error pada sensor ampere
              if($error_ampere=='0')
              {
                $pesan = "$pesan"." Ampere ERROR";
                // Mengambil data temporary
                $query_temp = $this->db->query("select ampere from local_chiller where device_code='$device_id' and penampung='1'");
                foreach($query_temp->result() as $row_temp)
                {
                  $ampere = $row_temp->ampere + $ampere;
                  
                }
              }else
              {
                $ampere = $teks[4] + $ampere;
              }

              //Hanya menghitung di luar buzzer
              if($teks[6] != 30)
              {
                $door = $door + $teks[7];         
              }
              
              //echo "$device_id | $pesan | $ec | $error_suhu_in | $error_suhu_out | $error_ampere | $error_humidity<br>";

              // $ip = $teks[8];
              $date_insert_local = $row2->created_date;  
                            
            }  

            $suhu_in = round($suhu_in / $no,1);
            $suhu_out = round($suhu_out / $no,1);
            $humidity = round($humidity/$no,1);
            $signal = round($signal/$no,1); 
            $ampere = round($ampere/$no,1); 
            $status_pintu = $teks[7];  

            $data = array(
            'device_code' => $device_id,
            'suhu_in'     => $suhu_in,
            'suhu_out'    => $suhu_out,
            'humidity'    => $humidity,
            'ampere'      => $ampere,
            'sinyal'      => $signal,
            'durasi'      => $durasi,
            'counter'     => $door,
            'date_insert_local' => $date_insert_local,
            'jumlah_data' => $no,
            'volt'        => $device_volt,
            'doors'       => $status_pintu,
            'keterangan'  => $pesan,
            );

            $data_temp = array(
            'device_code' => $device_id,
            'suhu_in'     => $suhu_in,
            'suhu_out'    => $suhu_out,
            'humidity'    => $humidity,
            'ampere'      => $ampere,
            'sinyal'      => $signal,
            'durasi'      => $durasi,
            'counter'     => $door,
            'date_insert_local' => $date_insert_local,
            'jumlah_data' => $no,
            'volt'        => $device_volt,
            'doors'       => $status_pintu,
            'penampung'   => 1,
            'keterangan'  => $pesan,
            );

            $insert =  $this->curl->simple_post($this->API.'/kontak', $data, array(CURLOPT_BUFFERSIZE => 10)); 
            $hapus_table = $this->db->query("delete from chiller where id in ($id)");
            $hapus_temp = $this->db->query("delete from local_chiller where penampung=1 and device_code='$row->device_code'");
            $masuk_local = $this->db->insert('local_chiller', $data_temp);
            if($insert=="") //JIka gagal kirim ke server
            {
              $masuk_local = $this->db->insert('local_chiller', $data);
            }
              

          // Jika sensor tidak mengirim data ke mini pc   
          }
          else
          { 
            $query_temp = $this->db->query("select * from local_chiller where device_code='$device_code' and penampung='1'");
            foreach($query_temp->result() as $row_temp)
            {
              $ampere = $row_temp->ampere;
              $suhu_in = $row_temp->suhu_in;
              $suhu_out = $row_temp->suhu_out;
              $humidity = $row_temp->humidity;
              $pesan = "Sensor WIFI ERROR";
            }
            $data = array(
              'device_code' => $device_code,
              'suhu_in'     => $suhu_in,
              'suhu_out'    => $suhu_out,
              'humidity'    => $humidity,
              'ampere'      => $ampere,
              'sinyal'      => 0,
              'durasi'      => 0,
              'counter'     => 0,
              'date_insert_local' => date('Y-m-d H:i:s'),
              'jumlah_data' => 0,
              'volt'        => 0,
              'doors'       => 0,
              'keterangan'  => $pesan,
            );
            $insert =  $this->curl->simple_post($this->API.'/kontak', $data, array(CURLOPT_BUFFERSIZE => 10)); 
            // echo $insert;
            
            if($insert=="")
            {
              $masuk_local = $this->db->insert('local_chiller', $data);
            }            
          }
          
          echo $insert;
        }
           
      }else{
        echo "<h1>Tidak Ada Alat Terinstall</h1>";
      }
       
      // Proses Mengambil data dari local chiller dan memasukkan data tsb ke server
      $query_local = $this->db->query("select * from local_chiller where penampung is null");
      $jumlah_data3 = $query_local->num_rows($query_local);
      if($jumlah_data3>=1)
      {
        foreach($query_local->result() as $row3)
        {
          $data = array(
          'device_code' => $row3->device_code,
          'suhu_in'     => $row3->suhu_in,
          'suhu_out'    => $row3->suhu_out,
          'humidity'    => $row3->humidity,
          'ampere'      => $row3->ampere,
          'sinyal'      => $row3->sinyal,
          'durasi'      => $row3->durasi,
          'counter'     => $row3->counter,
          'date_insert_local' => $row3->date_insert_local,
          'jumlah_data' => $row3->jumlah_data,
          'volt'        => $row3->volt,
          'doors'       => $row3->doors,
        );
          $insert2 =  $this->curl->simple_post($this->API.'/kontak', $data, array(CURLOPT_BUFFERSIZE => 10)); 
          // echo $insert2;
          if($insert2!="")
          {
            $hapus_local = $this->db->query("delete from local_chiller where id='$row3->id'");
          }          
        }
      }

      // Proses memindahkan instalasi local ke produksi server
      $query_fase = $this->db->query("select value from fase");
      foreach($query_fase->result() as $row4)
      {
        $fase = $row4->value;
      }     

      //Memindahkan data instalasi lokal ke server
      if($fase==5)
      {
        $query_penampung_produksi = $this->db->query("select * from penampung_produksi");
        foreach($query_penampung_produksi->result() as $row5)
        {
          $kode_toko = $row5->kode_toko;
          $data2 = array(
          'kode_vm'       => $row5->kode_vm,
          'kode_mini_pc'  => $row5->kode_mini_pc,
          'kode_toko'     => $row5->kode_toko,
          'kode_installer'=> $row5->kode_installer,
          'tanggal_instalasi' => $row5->tanggal_instalasi,
          );  
            $insert_produksi =  $this->curl->simple_post($this->API.'/produksi', $data2, array(CURLOPT_BUFFERSIZE => 10)); 
            echo "<br>";
            echo $insert_produksi;
            if($insert_produksi!="")
            {
              $hapus_produksi = $this->db->query("insert into recording (message,status) values ('Data Toko berhasil di kirim','sukses')");
              $update_fase = $this->db->query("update fase set value='6'");
            }
        }

        $query_sensor = $this->db->query("select * from data_chiller");
        foreach($query_sensor->result() as $row6)
        {
          $data3 = array(
          'device_code' => $row6->device_code,
          'volt'        => $row6->volt,
          'merk_unit'   => $row6->merk_unit,
          'type_unit'   => $row6->type_unit,
          'ampere'      => $row6->ampere,
          'kode_unit'   => $row6->kode_unit,
          'id_produksi' => $insert_produksi,
          ); 
          $insert_produksidetail = $this->curl->simple_post($this->API.'/produksidetail', $data3, array(CURLOPT_BUFFERSIZE => 10)); 
        }        
      }

      //Fungsi untuk sync data volt meter
      $suhu_in2 = 0;
      $volt_r = 0;
      $volt_s = 0;
      $volt_t = 0;
      $no2 = 0;
      $id2 = 0;
      $query3 = $this->db->query("select * from volt_meter");
      $jumlah_volt = $query3->num_rows();
      if($jumlah_volt>=1)
      {
        foreach($query3->result() as $row3)
        {
          $teks2 = explode("~", $row3->teks);
          $no2 = $no2 + 1;
          $id2 = "$id2".","."$row3->id";
          $device_id2 = $teks2[0];
          $suhu_in2 = $teks2[1] + $suhu_in2;
          $volt_r = $teks2[2] + $volt_r;
          $volt_s = $teks2[3] + $volt_s;
          $volt_t = $teks2[4] + $volt_t;
          $signal2 = $teks2[5];
          $ip2 = $teks2[6];
          $date_insert_local2 = $row3->created_date; 
        }  
          $suhu_in2 = $suhu_in2 / $no2;
          $volt_r = $volt_r / $no2;
          $volt_s = $volt_s / $no2;
          $volt_t = $volt_t / $no2;    

          $data3 = array(
            'device_code' => $device_id2,
            'suhu_in'     => $suhu_in2,
            'volt_r'    => $volt_r,
            'volt_s'    => $volt_s,
            'volt_t'      => $volt_t,
            'sinyal'      => $signal2,
            'ip'      => $ip2,
            'date_insert_local' => $date_insert_local2,
          );
          echo "<hr>";
            $insert_volt =  $this->curl->simple_post($this->API.'/Volt', $data3, array(CURLOPT_BUFFERSIZE => 10));
            if($insert_volt="")
            {
              $masuk_local_volt = $this->db->insert('local_volt_meter', $data3);
            }
            $hapus_local = $this->db->query("delete from volt_meter where id in ($id2)");
            echo $insert_volt;
      }
      
      $query_usage = $this->db->query("SELECT code, AVG(temperature) as temperature, AVG(memory) as memory, now() as datetime FROM pemakaian_daya");
      $jumlah_data_usage = $query_usage->num_rows($query_usage);
      if($jumlah_data_usage>=1) {
        foreach($query_usage->result() as $row_daya)
        {
          $data4 = array(
            'code'  => $row_daya->code,
            'temperature' => round($row_daya->temperature,0),
            'memory'  => round($row_daya->memory,0),
            'datetime'  => $row_daya->datetime,
            );
            $insert_usage = $this->curl->simple_post($this->API.'/Pemakaian', $data4, array(CURLOPT_BUFFERSIZE => 10));
            if($insert_usage!="")
            {
              $hapus_local = $this->db->query("delete from pemakaian_daya");
            } else {
              echo "<hr>Data Usage CPU dan Memory Gagal Dikirim !!!<br>";
            }
            echo "<br>";
            echo $insert_usage;
          }
        }

      $this->load->view('sync');
    } 
}
